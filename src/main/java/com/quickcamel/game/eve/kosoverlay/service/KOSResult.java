/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.quickcamel.game.eve.kosoverlay.service.dto.KOSPilotResultDTO;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Contains the results of a pilot KOS Check
 *
 * @author Louis Burton
 */
public class KOSResult {

    protected String m_pilotName;

    protected KOSStatusSummary m_statusSummary;

    protected KOSPilotResultDTO m_pilotDetails;

    public String getPilotName() {
        return m_pilotName;
    }

    public void setPilotName(String pilotName) {
        m_pilotName = pilotName;
    }

    public KOSStatusSummary getStatusSummary() {
        return m_statusSummary;
    }

    public void setStatusSummary(KOSStatusSummary statusSummary) {
        m_statusSummary = statusSummary;
    }

    public KOSPilotResultDTO getPilotDetails() {
        return m_pilotDetails;
    }

    public void setPilotDetails(KOSPilotResultDTO pilotDetails) {
        m_pilotDetails = pilotDetails;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("pilotName", m_pilotName).
                append("statusSummary", m_statusSummary).
                append("pilotDetails", m_pilotDetails).
                toString();
    }
}
