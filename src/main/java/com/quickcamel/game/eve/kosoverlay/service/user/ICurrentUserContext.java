/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.user;

import com.quickcamel.game.eve.kosoverlay.service.EntityStatus;
import com.quickcamel.game.eve.kosoverlay.service.dto.KOSPilotResultDTO;

/**
 * Description: Accesses information relevant to the current pilot using the KOS Overlay
 *
 * @author Louis Burton
 */
public interface ICurrentUserContext {

    String getPilotName();

    Long getCorporationId();

    Long getAllianceId();

    EntityStatus getStanding(Long eveId);

    boolean hasNegativeStandings(KOSPilotResultDTO pilot);
}
