/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.tasks;

import com.quickcamel.game.eve.kosoverlay.service.KOSResult;
import com.quickcamel.game.eve.kosoverlay.service.KOSStatusSummary;
import com.quickcamel.game.eve.kosoverlay.service.dto.KOSPilotResultDTO;
import javafx.concurrent.Task;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Description: Provides key functionality needed from a KOS check to update the view
 *
 * @author Louis Burton
 */
public abstract class KOSCheckTask extends Task<Void> {

    protected String m_pilotName;

    protected KOSResult m_kosResult;


    @Override
    protected Void call() throws Exception {
        try {
            doCheck();
        }
        finally {
            updateProgress(3L, 3L);
        }
        return null;
    }

    protected abstract void doCheck() throws Exception;

    /**
     * Only required until prototype bean creation with arguments via Java Spring Config is eased
     *
     * @param name of the pilot being checked
     */
    public void setPilotName(String name) {
        m_pilotName = name;
    }

    public String getPilotName() {
        return m_pilotName;
    }

    public KOSStatusSummary getKOSStatusSummary() {
        return m_kosResult == null ? null : m_kosResult.getStatusSummary();
    }

    public KOSPilotResultDTO getPilotKOSDetails() {
        return m_kosResult == null ? null : m_kosResult.getPilotDetails();
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("pilotName", m_pilotName).
                append("kosResult", m_kosResult).
                toString();
    }
}
