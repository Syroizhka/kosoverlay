/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view;

import com.quickcamel.game.eve.kosoverlay.configuration.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Description: Swing dialogue used due to JavaFX's current lack of always on top support
 * https://javafx-jira.kenai.com/browse/RT-153
 *
 * @author Louis Burton
 */
public class AlwaysOnTopDialogue extends JDialog implements IConfigChangeListener {

    private static final Logger m_logger = LoggerFactory.getLogger(AlwaysOnTopDialogue.class);
    private Timer m_alwaysOnTopTimer = new Timer("AlwaysOnTopTimer");
    private AtomicBoolean m_alwaysOnTopTimerRunning = new AtomicBoolean(false);
    private boolean m_settingsShowing = false;

    @Inject
    private IConfigManager m_configManager;

    private static final List<Image> ICONS = Arrays.asList(
            new ImageIcon(AlwaysOnTopDialogue.class.getResource("sev-logo-16.png")).getImage(),
            new ImageIcon(AlwaysOnTopDialogue.class.getResource("sev-logo-32.png")).getImage(),
            new ImageIcon(AlwaysOnTopDialogue.class.getResource("sev-logo-64.png")).getImage());

    public AlwaysOnTopDialogue() {
        super(new DummyFrame("KOS Overlay", ICONS));
        setTitle("KOS Overlay");
        setSize(600, 1000);
        setUndecorated(true);
        setBackground(new Color(0, 0, 0, 0));
        setAlwaysOnTop(true);
        setFocusableWindowState(false);
        getContentPane().setLayout(new java.awt.BorderLayout());
    }

    @PostConstruct
    public void updateAlwaysOnTopThread() {
        int alwaysOnTopDelay = Integer.valueOf(m_configManager.getValue(ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS, ConfigKeyConstants.OVERLAY_ALWAYSONTOP_DELAY_MS_DEFAULT));
        if (m_alwaysOnTopTimerRunning.getAndSet(false)) {
            m_logger.debug("Stopping alwaysOnTop timer");
            m_alwaysOnTopTimer.cancel();
            m_alwaysOnTopTimer = new Timer("AlwaysOnTopTimer");
        }
        if (alwaysOnTopDelay > 0 && m_alwaysOnTopTimerRunning.compareAndSet(false, true)) {
            m_logger.debug("Starting alwaysOnTop timer with a delay of : " + alwaysOnTopDelay);
            m_alwaysOnTopTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    AlwaysOnTopDialogue.this.resetAlwaysOnTop();
                }
            }, 0, alwaysOnTopDelay);
        }
    }

    private void resetAlwaysOnTop() {
        if (!m_settingsShowing) {
            setAlwaysOnTop(false);
            setAlwaysOnTop(true);
        }
    }

    public void setSettingsShowing(boolean settingsShowing) {
        m_settingsShowing = settingsShowing;
    }

    public void setDialogOpen(boolean dialogOpen) {
        if (dialogOpen) {
            setAlwaysOnTop(false);
        }
        else {
            setAlwaysOnTop(true);
        }
    }

    @Override
    public void notifyConfigChange() {
        updateAlwaysOnTopThread();
    }

    @Override
    public void toFront() {
    }

    @Override
    public void toBack() {
    }

    private static class DummyFrame extends JFrame {

        private DummyFrame(String title, List<? extends Image> iconImages) {
            super(title);
            setUndecorated(true);
            setVisible(true);
            setLocationRelativeTo(null);
            setIconImages(iconImages);

            addWindowStateListener(new WindowStateListener() {
                public void windowStateChanged(WindowEvent e) {
                    if (e.getNewState() == MAXIMIZED_BOTH || e.getNewState() == NORMAL) {
                        if (getOwnedWindows().length > 0) {
                            getOwnedWindows()[0].setVisible(false);
                            getOwnedWindows()[0].setVisible(true);
                        }
                    }
                }
            });
            setAutoRequestFocus(false);
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        }

        @Override
        public void setVisible(boolean visible) {
            super.setVisible(visible);
            if (visible && getOwnedWindows().length > 0) {
                getOwnedWindows()[0].setVisible(true);
            }
        }
    }
}
