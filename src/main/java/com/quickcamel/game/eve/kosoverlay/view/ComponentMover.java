/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view;

import com.quickcamel.game.eve.kosoverlay.configuration.ConfigKeyConstants;
import com.quickcamel.game.eve.kosoverlay.view.fx.controllers.OverlayController;
import com.quickcamel.game.eve.kosoverlay.configuration.IConfigManager;
import javafx.application.Platform;
import javafx.scene.control.ToggleButton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.*;
import javax.inject.Inject;
import javax.swing.JComponent;

/**
 * This class allows you to move a Component by using a mouse. The Component
 * moved can be a high level Window (ie. Window, Frame, Dialog) in which case
 * the Window is moved within the desktop. Or the Component can belong to a
 * Container in which case the Component is moved within the Container.
 * <p/>
 * When moving a Window, the listener can be added to a child Component of
 * the Window. In this case attempting to move the child will result in the
 * Window moving. For example, you might create a custom "Title Bar" for an
 * undecorated Window and moving of the Window is accomplished by moving the
 * title bar only.
 */
public class ComponentMover extends MouseAdapter {

    private static final Logger m_logger = LoggerFactory.getLogger(ComponentMover.class);

    private Insets m_dragInsets = new Insets(0, 0, 0, 0);
    private Dimension m_snapSize = new Dimension(1, 1);
    private Insets m_edgeInsets = new Insets(0, 0, 0, 0);
    private boolean m_changeCursor = true;

    private Component m_destinationComponent;
    private Component m_source;

    private Point m_pressed;
    private Point m_location;

    private boolean m_autoScrolls;
    private boolean m_potentialDrag;

    private ToggleButton m_anchorButton;
    private boolean m_anchored;

    @Inject
    private IConfigManager m_configManager;

    @Inject
    private OverlayController m_overlayController;

    /**
     * Constructor to specify a parent component that will be moved when drag
     * events are generated on a registered child component.
     *
     * @param destinationComponent the component drag events should be forwarded to
     * @param component            the Component to be registered for forwarding drag
     *                             events to the parent component to be moved
     */
    public ComponentMover(Component destinationComponent, Component component) {
        this.m_destinationComponent = destinationComponent;
        this.m_source = component;
        registerComponent(component);
    }

    /**
     * Get the drag insets
     *
     * @return the drag insets
     */
    public Insets getDragInsets() {
        return m_dragInsets;
    }

    /**
     * Set the drag insets. The insets specify an area where mouseDragged
     * events should be ignored and therefore the component will not be moved.
     * This will prevent these events from being confused with a
     * MouseMotionListener that supports component resizing.
     *
     * @param dragInsets
     */
    public void setDragInsets(Insets dragInsets) {
        this.m_dragInsets = dragInsets;
    }

    /**
     * Get the bounds insets
     *
     * @return the bounds insets
     */
    public Insets getEdgeInsets() {
        return m_edgeInsets;
    }

    /**
     * Set the edge insets. The insets specify how close to each edge of the parent
     * component that the child component can be moved. Positive values means the
     * component must be contained within the parent. Negative values means the
     * component can be moved outside the parent.
     *
     * @param edgeInsets
     */
    public void setEdgeInsets(Insets edgeInsets) {
        this.m_edgeInsets = edgeInsets;
    }

    /**
     * Add the required listeners to the specified component
     *
     * @param component the component the listeners are added to
     */
    public void registerComponent(Component component) {
        this.m_source = component;
        component.addMouseListener(this);
    }

    /**
     * Get the snap size
     *
     * @return the snap size
     */
    public Dimension getSnapSize() {
        return m_snapSize;
    }

    /**
     * Set the snap size. Forces the component to be snapped to
     * the closest grid position. Snapping will occur when the mouse is
     * dragged half way.
     */
    public void setSnapSize(Dimension snapSize) {
        if (snapSize.width < 1
                || snapSize.height < 1) {
            throw new IllegalArgumentException("Snap sizes must be greater than 0");
        }

        this.m_snapSize = snapSize;
    }

    /**
     * Setup the variables used to control the moving of the component:
     * <p/>
     * source - the source component of the mouse event
     * destination - the component that will ultimately be moved
     * pressed - the Point where the mouse was pressed in the destination
     * component coordinates.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        if (m_anchorButton == null) {
            m_logger.error("No anchor button has been registered");
        }
        else {
            if (!m_anchored) {
                m_source = e.getComponent();
                int width = (int) m_anchorButton.getWidth() - m_dragInsets.left - m_dragInsets.right;
                int height = (int) m_anchorButton.getHeight() - m_dragInsets.top - m_dragInsets.bottom;
                Rectangle r = new Rectangle(m_dragInsets.left, m_dragInsets.top, width, height);

                if (r.contains(e.getPoint())) {
                    setupForDragging(e);
                }
            }
        }
    }

    private void setupForDragging(MouseEvent e) {
        m_source.addMouseMotionListener(this);
        m_potentialDrag = true;

        m_pressed = e.getLocationOnScreen();
        m_location = m_destinationComponent.getLocation();

        //  Making sure m_autoScrolls is false will allow for smoother dragging of
        //  individual components
        if (m_destinationComponent instanceof JComponent) {
            JComponent jc = (JComponent) m_destinationComponent;
            m_autoScrolls = jc.getAutoscrolls();
            jc.setAutoscrolls(false);
        }
    }

    /**
     * Move the component to its new location. The dragged Point must be in
     * the destination coordinates.
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        Point dragged = e.getLocationOnScreen();
        int dragX = getDragDistance(dragged.x, m_pressed.x, m_snapSize.width);
        int dragY = getDragDistance(dragged.y, m_pressed.y, m_snapSize.height);

        int locationX = m_location.x + dragX;
        int locationY = m_location.y + dragY;

        //  Don't contain the component within the main display only

        if (m_anchorButton != null) {
            m_anchorButton.setDisable(true);
            if (m_changeCursor) {
                m_source.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            }
        }

        //  Adjustments are finished, move the component
        m_destinationComponent.setLocation(locationX, locationY);
    }

    /*
     *  Determine how far the mouse has moved from where dragging started
     *  (Assume drag direction is down and right for positive drag distance)
     */
    private int getDragDistance(int larger, int smaller, int snapSize) {
        int halfway = snapSize / 2;
        int drag = larger - smaller;
        drag += (drag < 0) ? -halfway : halfway;
        drag = (drag / snapSize) * snapSize;

        return drag;
    }

    /**
     * Restore the original state of the Component
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        if (!m_potentialDrag) {
            return;
        }
        if (m_anchorButton.isDisabled()) {
            m_anchorButton.setDisable(false);
            m_anchorButton.setSelected(!m_anchorButton.isSelected());
            m_configManager.setValue(ConfigKeyConstants.POSITION_X, String.valueOf((int) m_destinationComponent.getLocation().getX()));
            m_configManager.setValue(ConfigKeyConstants.POSITION_Y, String.valueOf((int) m_destinationComponent.getLocation().getY()));
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    m_overlayController.removeHelp(true);
                }
            });
        }

        m_source.removeMouseMotionListener(this);
        m_potentialDrag = false;

        if (m_changeCursor) {
            m_source.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        if (m_destinationComponent instanceof JComponent) {
            ((JComponent) m_destinationComponent).setAutoscrolls(m_autoScrolls);
        }
    }

    public void registerAnchorButton(ToggleButton button) {
        this.m_anchorButton = button;
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle bounds = env.getMaximumWindowBounds();
        this.m_destinationComponent.setLocation(
                Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.POSITION_X, Integer.toString((bounds.width / 2) - (int) m_anchorButton.getMinWidth()))),
                Integer.parseInt(m_configManager.getValue(ConfigKeyConstants.POSITION_Y, Integer.toString((bounds.height / 2) - (int) m_anchorButton.getMinHeight() - 200))));
    }

    public void setAnchored(boolean anchored) {
        this.m_anchored = anchored;
    }
}