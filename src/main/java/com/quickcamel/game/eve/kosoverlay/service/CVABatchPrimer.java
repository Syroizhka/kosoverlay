/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service;

import com.google.common.cache.LoadingCache;
import com.quickcamel.game.eve.kosoverlay.configuration.*;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.service.rest.IRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.*;

/**
 * Uses the CVA KOS checker's 'multi' option to prime the pilot KOS cache
 *
 * @author Louis Burton
 */
public class CVABatchPrimer implements IBatchPrimer {

    private static final Logger m_logger = LoggerFactory.getLogger(CVABatchPrimer.class);

    @Inject
    private IConfigManager m_configManager;

    @Inject
    private IRestClient m_restClient;

    @Inject
    private Version m_version;

    @Resource(name = "pilotCVACache")
    private LoadingCache<String, KOSResultContainerDTO> m_pilotCVACache;

    @Override
    public void prime(Collection<String> pilots) {
        m_logger.debug("Priming pilot checks in batch.");
        StringBuilder commaPilotList = new StringBuilder(pilots.size() * 10);
        for (String pilot : pilots) {
            if (m_pilotCVACache.getIfPresent(pilot.trim()) == null) {
                if (commaPilotList.length() > 0) {
                    commaPilotList.append(',');
                }
                commaPilotList.append(pilot.trim());
            }
        }
        if (commaPilotList.length() == 0) {
            m_logger.debug("Batch priming has no non-cached pilots to check.");
        }
        else {
            KOSResultContainerDTO result;
            try {
                Map<String, List<String>> queryParams = new HashMap<>();
                queryParams.put("c", Arrays.asList("json"));
                queryParams.put("type", Arrays.asList("multi"));
                queryParams.put("icon", Arrays.asList("32"));
                queryParams.put("max", Arrays.asList(m_configManager.getValue(ConfigKeyConstants.CVABATCHPRIMER_MAX, ConfigKeyConstants.CVABATCHPRIMER_MAX_DEFAULT)));
                queryParams.put("offset", Arrays.asList("0"));
                queryParams.put("q", Arrays.asList(commaPilotList.toString()));
                Map<String, String> headerParams = new HashMap<>();
                headerParams.put(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
                headerParams.put(HttpHeaders.USER_AGENT, m_version.getUserAgent());
                result = m_restClient.get("http://kos.cva-eve.org/api/", queryParams, headerParams, KOSResultContainerDTO.class);
                if (result.getTotal() <= 0
                        || result.getResults() == null
                        || result.getResults().size() == 0) {
                    m_logger.debug("No results from batch priming.");
                }
                else {
                    for (KOSResultDTO kosResult : result.getResults()) {
                        if (kosResult.getType().equals("pilot")) {
                            KOSPilotResultDTO pilotResult = (KOSPilotResultDTO) kosResult;
                            final KOSResultContainerDTO containerResult = new KOSResultContainerDTO();
                            containerResult.setTotal(1);
                            containerResult.setCode(100);
                            containerResult.setResults(Arrays.asList(new KOSResultDTO[]{pilotResult}));
                            m_pilotCVACache.put(kosResult.getLabel(), containerResult);
                            m_logger.debug("Put result for : " + kosResult.getLabel() + ", object : " + kosResult);
                        }
                    }
                }
            }
            catch (Exception e) {
                if (e.getCause() instanceof WebApplicationException) {
                    CVARESTLogger.handleWebApplicationException("Batch " + commaPilotList, (WebApplicationException) e.getCause());
                }
                m_logger.error("API Check for " + commaPilotList + " failed.", e);
            }
        }
    }
}
